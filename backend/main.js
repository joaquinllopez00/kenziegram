// ESM syntax is supported.
//import express
import express from "express";
//import multer (demo on uploading files using it)
import multer from "multer";

const app = express();

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

//listening on port 6000
app.listen(6000, () => {
  console.log("express server is running on port 6000");
});
